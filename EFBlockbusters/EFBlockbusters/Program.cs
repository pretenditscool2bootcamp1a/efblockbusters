﻿

using EFBlockbusters.Models;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Database;

namespace EFBlockbusters
{

    public class program
    {

        static void Main(string[] args)
        {
            
            Console.WriteLine("Welcome to Blockbuster. The choice for movie rentals that will never be beaten");
            while (true)
            {
                Console.WriteLine("1. Movie Categories");
                Console.WriteLine("2. Search for a Movie");
                Console.WriteLine("3. Display movies");
                int command = int.Parse(Console.ReadLine());

                switch(command)
                {
                    case 1:
                        categoryMenu();
                        break;
                    case 2:
                        searchMovie();
                        break;
                    case 3:
                        displayMovies(0);
                        break;
                    default:
                        Console.WriteLine("Invalid choice. Please try again.");
                        break;
                }
            }
        }
        public static void categoryMenu()
        {
            
            using (var context = new EfcodeFirstContext())
            {
                var allCategories = from c in context.Categories select c;
                foreach(var category in allCategories)
                {
                    Console.WriteLine($"Category id: {category.CategoryId}  Category Name: {category.CategoryName}");
                }
            }
            Console.WriteLine();
            displayMoviesByCategory();
        }

        public static void searchMovie()
        {
            var context = new EfcodeFirstContext();
        }

        public static void displayMovies(int movieNumber)
        {
            var context = new EfcodeFirstContext();
        }

        public static void displayMoviesByCategory()
        {
            Console.WriteLine("Enter Category Name to display Movies:");
            var context = new EfcodeFirstContext();
            int categoryChoice = int.Parse(Console.ReadLine());
            var allMovies= from m in context.Movies
                              where m.Category.CategoryId == categoryChoice
                              
                              select new
                              {
                                  m.MovieId,
                                  m.MovieName,
                                  m.Description
                              };
            foreach (var m in allMovies)
            {
                Console.WriteLine($"Movie Name:{m.MovieName }");
                Console.WriteLine($"Movie Description:{m.Description}");
                Console.WriteLine();
            }
        }
    }
}