﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFBlockbusters
{
    internal class Connections
    {
        public static string GetConnectionString()
        {
            ConfigurationBuilder builder = new ConfigurationBuilder();
            builder.SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json", optional: false,
            reloadOnChange: true);

            IConfiguration config = builder.Build();

            string? connStr = config["ConnectionStrings:Northwind"];
            /*if (connStr != null)
            {
                Console.WriteLine(connStr);
            }*/
            return connStr;
        }
    }
}
