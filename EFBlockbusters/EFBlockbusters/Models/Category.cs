﻿using System;
using System.Collections.Generic;

namespace EFBlockbusters.Models;

public partial class Category
{
    public int CategoryId { get; set; }

    public string CategoryName { get; set; } = null!;

    public virtual ICollection<Movie> Movies { get; } = new List<Movie>();
}
