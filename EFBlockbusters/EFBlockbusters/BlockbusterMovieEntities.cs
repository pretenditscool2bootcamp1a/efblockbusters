﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFBlockbusters
{
    public class BlockbusterMovieEntities : DbContext
    {
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Category> Categories { get; set; }
        //string connectionString = Connections.GetConnectionString();
        string connectionString = "Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=EFCodeFirst;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
        protected override void
            OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(connectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>()
                .HasMany(category => category.Movies)
                .WithOne(movie => movie.Category)
                .HasForeignKey(movie => movie.CategoryID);

            modelBuilder.Entity<Category>().HasData(
                new Category
                {
                    CategoryID = 1,
                    CategoryName = "Science Fiction"
                },
                new Category
                {
                    CategoryID = 2,
                    CategoryName = "Horror"
                },
                new Category
                {
                    CategoryID = 3,
                    CategoryName = "Dark Comedy"

                },
                 new Category
                 {
                     CategoryID = 4,
                     CategoryName = "Dark Fantasy"

                 }

                );
            modelBuilder.Entity<Movie>().HasData(
                new Movie
                 {
                    MovieID = 1,
                    MovieName = "In Bruges",
                    Description = @"After a job gone wrong, hitman Ray and his partner await orders from their ruthless boss in Bruges, Belgium, the last place in the world Ray wants to be.",
                  /*  Category = "Dark Comedy",*/
                    CategoryID = 3
                 },
                 new Movie
                 {
                     MovieID = 2,
                     MovieName = "Blade Runner",
                     Description = @"A blade runner must pursue and terminate four replicants who stole a ship in space and have returned to Earth to find their creator.",
                    /* Category = "Science Fiction",*/
                     CategoryID = 1
                 },
                 new Movie
                 {
                     MovieID = 3,
                     MovieName = "Blade Runner 2049",
                     Description = @"Young Blade Runner K's discovery of a long-buried secret leads him to track down former Blade Runner Rick Deckard, who's been missing for thirty years.",
                     /*Category = "Science Fiction",*/
                     CategoryID = 1
                 },
                 new Movie
                 {
                     MovieID = 4,
                     MovieName = "La Cité des Enfants Perdus",
                     Description = @"A scientist in a surrealist society kidnaps children to steal their dreams, hoping that they slow his aging process.",
                     /*Category = "Dark Fantasy",*/
                     CategoryID = 4
                 },
                 new Movie
                 {
                     MovieID = 5,
                     MovieName = "Midsommar",
                     Description = @"A couple travels to Northern Europe to visit a rural hometown's fabled Swedish mid-summer festival. What begins as an idyllic retreat quickly devolves into an increasingly violent and bizarre competition at the hands of a pagan cult.",
                     /*Category = "Dark Fantasy",*/
                     CategoryID = 4
                 },
                 new Movie
                 {
                     MovieID = 6,
                     MovieName = "What We Do in the Shadows",
                     Description = @"Viago, Deacon, and Vladislav are vampires who are struggling with the mundane aspects of modern life, like paying rent, keeping up with the chore wheel, trying to get into nightclubs, and overcoming flatmate conflicts.",
                     /*Category = "Dark Comedy",*/
                     CategoryID = 3
                 },
                 new Movie
                 {
                     MovieID = 7,
                     MovieName = "The Evil Dead",
                     Description = @"Five friends travel to a cabin in the woods, where they unknowingly release flesh-possessing demons.",
                     /*Category = "Horror",*/
                     CategoryID = 2
                 },
                  new Movie
                  {
                      MovieID = 8,
                      MovieName = "Jacob's Ladder",
                      Description = @"Mourning his dead child, a haunted Vietnam War veteran attempts to uncover his past while suffering from a severe case of dissociation. To do so, he must decipher reality and life from his own dreams, delusions, and perceptions of death.",
                      /*Category = "Horror",*/
                      CategoryID = 2
                  }
 );
        }
    }
}
