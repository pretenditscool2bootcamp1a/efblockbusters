﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace EFBlockbusters.Migrations
{
    /// <inheritdoc />
    public partial class SeedInitialCats : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "CategoryID",
                keyValue: 4,
                column: "CategoryName",
                value: "Dark Fantasy");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "CategoryID",
                keyValue: 4,
                column: "CategoryName",
                value: "Western");
        }
    }
}
