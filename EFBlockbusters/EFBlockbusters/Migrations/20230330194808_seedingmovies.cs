﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace EFBlockbusters.Migrations
{
    /// <inheritdoc />
    public partial class seedingcategories : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
           /* migrationBuilder.CreateTable(
                name: "Categories",
                columns: table => new
                {
                    CategoryID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CategoryName = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Categories", x => x.CategoryID);
                });*/

/*            migrationBuilder.CreateTable(
                name: "Movies",
                columns: table => new
                {
                    MovieID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MovieName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(120)", maxLength: 120, nullable: false),
                    CategoryID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Movies", x => x.MovieID);
                    table.ForeignKey(
                        name: "FK_Movies_Categories_CategoryID",
                        column: x => x.CategoryID,
                        principalTable: "Categories",
                        principalColumn: "CategoryID",
                        onDelete: ReferentialAction.Cascade);
                });*/

           /* migrationBuilder.InsertData(
                table: "Categories",
                columns: new[] { "CategoryID", "CategoryName" },
                values: new object[,]
                {
                    { 1, "Science Fiction" },
                    { 2, "Horror" },
                    { 3, "Dark Comedy" },
                    { 4, "Dark Fantasy" }
                });*/

            migrationBuilder.InsertData(
              table: "Mvoies",
              columns: new[] { "MovieID", "MovieName", "Description", "CategoryID" },
              values: new object[,]
              {
                    {
                     1,
                     "In Bruges",
                     "Guilt-stricken after a job gone wrong, hitman Ray and his partner await orders from their ruthless boss in Bruges, Belgium, the last place in the world Ray wants to be.",
                     3
                 },
                 
                 {
                     2,
                     "Blade Runner",
                     "A blade runner must pursue and terminate four replicants who stole a ship in space and have returned to Earth to find their creator.",
                     1
                 },
                 
                 {
                     3,
                     "Blade Runner 2049",
                     "Young Blade Runner K's discovery of a long-buried secret leads him to track down former Blade Runner Rick Deckard, who's been missing for thirty years.",
                     1
                 },
                 
                 {
                     4,
                     "La Cité des Enfants Perdus",
                     "A scientist in a surrealist society kidnaps children to steal their dreams, hoping that they slow his aging process.",
                     4
                 },
                 
                 {
                     5,
                     "Midsommar",
                     "A couple travels to Northern Europe to visit a rural hometown's fabled Swedish mid-summer festival. What begins as an idyllic retreat quickly devolves into an increasingly violent and bizarre competition at the hands of a pagan cult.",
                     4
                 },
                 
                 {
                     6,
                     "What We Do in the Shadows",
                     "Viago, Deacon, and Vladislav are vampires who are struggling with the mundane aspects of modern life, like paying rent, keeping up with the chore wheel, trying to get into nightclubs, and overcoming flatmate conflicts.",
                     3
                 },
                 
                 {
                     7,
                     "The Evil Dead",
                     "Five friends travel to a cabin in the woods, where they unknowingly release flesh-possessing demons.",
                     2
                 },
                 
                  {
                      8,
                      "Jacob's Ladder",
                      "Mourning his dead child, a haunted Vietnam War veteran attempts to uncover his past while suffering from a severe case of dissociation. To do so, he must decipher reality and life from his own dreams, delusions, and perceptions of death.",
                      2
                  }
              });


            /*  migrationBuilder.CreateIndex(
                  name: "IX_Movies_CategoryID",
                  table: "Movies",
                  column: "CategoryID");*/
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Movies");

          /*  migrationBuilder.DropTable(
                name: "Categories");*/
        }
    }
}
